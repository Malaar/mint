# Mint (Caching of remote resources)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for overview and testing purposes.

### Prerequisites

* Mac OS X 10.13+, Xcode 9.3+

### Installing

1. Clone Mint repository

			git clone git@bitbucket.org:Malaar/mint.git

2. Done.

## Logic details

Project was created with paradigm of Protocol Oriented Programming.
Project demonstrate work with MVVM pattern, multithreading and other.

### Usage:
* Use methods of class ResourceCache to get remote resource (fetch, cache will work under hood)
* Use ImageCache to get remote images (fetch, cache will work under hood)
* To change received resource before store it - create subclass of ProcessResourceOperation and send it via one of methods of ResourceCache/ImageCache:

### Customizations:
* Check configurations firstly (see: ResourceGateway, InMemoryStorageConfiguration, ResourceCache), maybe they copntains everything which you need to customize
* To customize work with network - create own class and adopt Gatewayable protocol, setup instance of this new class during ResourceCache initialization
* To customize way of data storing - create own class and adopt Storageable protocol, setup instance of this new class during ResourceCache initialization
* To customize or integrate some processing/changing of received resource into pipline - use subclass of ProcessResourceOperation
* To customize remote resource object - subclass CachableData or create own class and adopt ResourceCachable protocol (Notice: gateway will alway return resource as instance of CachableData, you alway can change it in one of your process operations)

## Author

* **[Andrew Korshilovskiy](http://www.linkedin.com/in/korshilovskiy)** - development

## License

(c) All rights reserved.
This project is licensed under the **Proprietary Software License** - see the [LICENSE](http://www.binpress.com/license/view/l/358023be402acff778a934083b76b86f) url for details
