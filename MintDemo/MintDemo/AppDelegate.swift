//
//  AppDelegate.swift
//  MintDemo
//
//  Created by Malaar on 6/5/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit
import Mint

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}

