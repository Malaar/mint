//
//  PinboardMapperJSON.swift
//  MintDemo
//
//  Created by Malaar on 6/6/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation

//MARK: - PinboardMapperJSON

class PinboardMapperJSON: PinboardMapper {
 
    func pinboardFromObject(_ object: Any) throws -> Pinboard {
        guard let dictionary = object as? Dictionary<String, Any> else {
            throw PinboardServiceError.parseObject
        }
        guard let urls = dictionary["urls"] as? Dictionary<String, Any> else {
            throw PinboardServiceError.parseObject
        }
        guard let smallPhoto = urls["small"] as? String, let smallPhotoUrl = URL(string: smallPhoto) else {
            throw PinboardServiceError.parseObject
        }
        guard let regularPhoto = urls["regular"] as? String, let regularPhotoUrl = URL(string: regularPhoto) else {
            throw PinboardServiceError.parseObject
        }
        
        let pinboard = Pinboard(smallPhotoURL: smallPhotoUrl, regularPhotoURL: regularPhotoUrl)
        return pinboard
    }
}
