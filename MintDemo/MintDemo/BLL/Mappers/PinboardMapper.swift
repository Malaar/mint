//
//  PinboardMapper.swift
//  MintDemo
//
//  Created by Malaar on 6/6/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - PinboardMapper protocol

protocol PinboardMapper {

    func pinboardFromObject(_ object: Any) throws -> Pinboard
}
