//
//  SharedCache.swift
//  MintDemo
//
//  Created by Malaar on 6/5/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation
import Mint


//MARK: - SharedCache

class SharedCache: ResourceCache {
    
    static let instance: SharedCache = {
//        let storage = InMemoryStorage(configuration: InMemoryStorageConfiguration.default)
        let storage = InMemoryStorage(configuration: InMemoryStorageConfiguration(maxCapacity: 100, maxMemoryUsage: 0))
        let gateway = ResourceGateway()
        let cache = SharedCache(gateway: gateway, storage: storage, maxConcurrentOperationCount: 10)
        return cache
    }()
}
