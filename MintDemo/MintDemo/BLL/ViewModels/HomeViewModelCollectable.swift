//
//  HomeViewModelCollectable.swift
//  MintDemo
//
//  Created by Malaar on 6/5/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation
import Mint


//MARK: - HomeViewModelCollectable protocol

protocol HomeViewModelCollectable {

    typealias ReloadCallback = (Bool, [IndexPath]?, Error?) -> Void

    var reloadCallback: ReloadCallback? {get set}
    var pageSize: Int {get set}
    
    func itemsCount() -> Int
    func item(at indexPath: IndexPath) -> ResourceCellViewModelable?
    func fetchData(reload: Bool) -> Void
}
