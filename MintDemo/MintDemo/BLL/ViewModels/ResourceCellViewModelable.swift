//
//  ResourceCellViewModelable.swift
//  MintDemo
//
//  Created by Malaar on 6/5/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation
import Mint


//MARK: - ResourceCellViewModelable protocol

protocol ResourceCellViewModelable {

    typealias LoadCallback = (UIImage?, Error?) -> Void
    
    var loadCallback: LoadCallback? { get set }

    func loadRemoteResources()
    func cancelLoadingRemoteResources()
}
