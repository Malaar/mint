//
//  HomeViewModel.swift
//  MintDemo
//
//  Created by Malaar on 6/5/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation
import Mint


//MARK: - HomeViewModel

class HomeViewModel: HomeViewModelCollectable {
    
    fileprivate struct Constants {
        static let pageSize = 30
    }

    var reloadCallback: ReloadCallback?
    var pageSize: Int = Constants.pageSize
    private var currentPage = 0
    private var pagesCompleted = false
    private var cellModels = [ResourceCellViewModelable]()
    private let pinboardService: PinboardHomable
    private let resourceCache: ResourceCache

    init(pinboardService: PinboardHomable, resourceCache: ResourceCache) {
        self.pinboardService = pinboardService
        self.resourceCache = resourceCache
    }
}


//MARK: - HomeViewModelCollectable

extension HomeViewModel {
    
    func itemsCount() -> Int {
        return cellModels.count
    }
    
    func item(at indexPath: IndexPath) -> ResourceCellViewModelable? {
        return indexPath.row < cellModels.count ? cellModels[indexPath.row] : nil
    }
    
    func fetchData(reload: Bool) -> Void {
        if !reload && pagesCompleted { return }

        if reload {
            currentPage = 0
            pagesCompleted = false
            cellModels.removeAll()
        }
        
        pinboardService.homePinboards(offset: currentPage, limit: pageSize) { [weak self] (pinboards, error) in
            guard let strongSelf = self else { return }
            
            if let error = error {
                print("Error loading pinboards: " + error.localizedDescription)
                DispatchQueue.main.async { [weak self] in
                    self?.reloadCallback?(reload, nil, error)
                }
                return
            }
            
            strongSelf.currentPage += 1
            let currentPageSize = pinboards?.count ?? 0
            if currentPageSize < strongSelf.pageSize {
                strongSelf.pagesCompleted = true
            }
            
            let insertFromIndex = strongSelf.cellModels.count
            pinboards?.forEach({ pinboard in
                let viewModel = ResourceCellViewModel(pinboard: pinboard, resourceCache: strongSelf.resourceCache)
                strongSelf.cellModels.append(viewModel)
            })
            let insertToIndex = max(strongSelf.cellModels.count - 1, 0)
            let insertIndexPaths = Array(insertFromIndex...insertToIndex).map{ IndexPath(item: $0, section: 0) }

            DispatchQueue.main.async { [weak self] in
                self?.reloadCallback?(reload, insertIndexPaths, error)
            }
        }
    }
}
