//
//  ResourceCellViewModel.swift
//  MintDemo
//
//  Created by Malaar on 6/5/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation
import Mint


//MARK: - ResourceCellViewModel

class ResourceCellViewModel: ResourceCellViewModelable {
    
    var loadCallback: LoadCallback?

    private let pinboard: Pinboard
    private let resourceCache: ResourceCache
    private var token: ResourceToken?
    
    init(pinboard: Pinboard, resourceCache: ResourceCache) {
        self.pinboard = pinboard
        self.resourceCache = resourceCache
    }
}


//MARK: - ResourceCellViewModelable

extension ResourceCellViewModel {
    
    func loadRemoteResources() {
        token?.cancel()
        token = resourceCache.resource(forURL: pinboard.smallPhotoURL) { [weak self] (resource, error) in
            self?.token = nil
            var photoImage: UIImage?
            if let data = resource?.data {
                //TODO: use ImageCache instead
                photoImage = UIImage(data: data)
                if photoImage == nil {
                    print("ERROR IMAGE CREATION: " + (self?.pinboard.smallPhotoURL.absoluteString)! )
                }
            } else {
                print("RESOURCE ERROR: Resource or data is nil!")
            }
            DispatchQueue.main.async { [weak self] in
                if let error = error {
                    print("HTTP ERROR: \(error.localizedDescription)")
                }
                self?.loadCallback?(photoImage, error)
            }
        }
    }
    
    func cancelLoadingRemoteResources() {
        token?.cancel()
        token = nil
    }
}
