//
//  Pinboard.swift
//  MintDemo
//
//  Created by Malaar on 6/6/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: Pinboard

class Pinboard {
    
    let smallPhotoURL: URL
    let regularPhotoURL: URL
    
    init(smallPhotoURL: URL, regularPhotoURL: URL) {
        self.smallPhotoURL = smallPhotoURL
        self.regularPhotoURL = regularPhotoURL
    }
}
