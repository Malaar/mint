//
//  GUITheme.swift
//  MintDemo
//
//  Created by Malaar on 5/05/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit

/**
 * App UI themes is here
 **/
struct GUITheme {
    
    static let `default` = GUITheme()
    
    private init() { }
    
    let mintColor = UIColor(hex: "24FBCA")
}
