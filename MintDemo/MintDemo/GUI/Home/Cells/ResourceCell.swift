//
//  ResourceCell.swift
//  MintDemo
//
//  Created by Malaar on 6/5/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit
import Mint


//MARK: - ResourceCell

class ResourceCell: UICollectionViewCell {
        
    struct Constants {
        static let reuseIdentifier = "ResourceCell"
        static let thumbnailImageName = "ImageThumbnail"
    }
    
    private(set) var viewModel: ResourceCellViewModelable?
    @IBOutlet private weak var imageView: UIImageView!
    
    func configure(with viewModel: ResourceCellViewModelable?) {
        self.viewModel = viewModel
        
        self.viewModel?.loadCallback = { [weak self] image, _ in
            self?.imageView.alpha = 1.0
            self?.imageView.image = image ?? UIImage(named: Constants.thumbnailImageName)
        }
        imageView.image = UIImage(named: Constants.thumbnailImageName)
        imageView.alpha = (100.0 + CGFloat(arc4random_uniform(128))) / 255.0
        
        viewModel?.loadRemoteResources()
    }

    func didEndDisplaying() {
        viewModel?.cancelLoadingRemoteResources()
    }
}
