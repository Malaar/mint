//
//  HomeController.swift
//  MintDemo
//
//  Created by Malaar on 6/5/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import UIKit


//MARK: - Constants

fileprivate struct Constants {
 
    struct CollectionLayout {
        
        struct Phone {
            static let itemMargin: CGFloat = 10.0
            static let columnsNumberPortrait = 3
            static let columnsNumberLandscape = 5
        }
        
        struct Pad {
            static let itemMargin: CGFloat = 20.0
            static let columnsNumberPortrait = 5
            static let columnsNumberLandscape = 6
        }
        
        static func itemMargin(traitCollection: UITraitCollection) -> CGFloat {
            return traitCollection.userInterfaceIdiom == .phone ? Phone.itemMargin : Pad.itemMargin
        }
        
        static func columnsNumber(traitCollection: UITraitCollection) -> Int {
            if traitCollection.userInterfaceIdiom == .phone {
                return UIScreen.main.isPortraitOrientation ? Phone.columnsNumberPortrait : Phone.columnsNumberLandscape
            } else {
                if traitCollection.horizontalSizeClass == .compact {
                    return Pad.columnsNumberPortrait
                } else {
                    return UIScreen.main.isPortraitOrientation ? Pad.columnsNumberPortrait : Pad.columnsNumberLandscape
                }
            }
        }
    }
}


//MARK: - HomeController

class HomeController: UICollectionViewController {

    private var viewModel: HomeViewModelCollectable?

    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewModel = createViewModel()
        setupViewModelCallbacks()
        setupCollection()
        fetchData(reload: true)
    }


    //MARK: - Configuration
    
    func createViewModel() -> HomeViewModelCollectable {
        // Use Dependency injector in real project
//        let url = URL(string: "https://pastebin.com/raw/wgkJgazE")!
        let url = URL(string: "https://pastebin.com/raw/XkEnZex4")!
        let mapper = PinboardMapperJSON()
        let pinboardService = PinboardHomeService(baseURL: url, mapper: mapper)
        return HomeViewModel(pinboardService: pinboardService, resourceCache: SharedCache.instance)
    }
    
    func setupViewModelCallbacks() {
        viewModel?.reloadCallback = { [weak self] (reload, insertIndexPaths, error) in
            if let refreshing = self?.collectionView?.refreshControl?.isRefreshing, refreshing == true {
                self?.collectionView?.refreshControl?.endRefreshing()
            }
            if(error != nil) {
//                HUD.flash(.error, onView: self?.view, delay: 1.0, completion: nil)
            } else {
                if reload {
//                    HUD.hide(animated: true)
                    self?.collectionView?.reloadData()
                } else if let insertIndexPaths = insertIndexPaths{
                    self?.collectionView?.insertItems(at: insertIndexPaths)
                }
            }
        }
    }

    func setupCollection() {
        self.collectionView?.refreshControl = UIRefreshControl()
        self.collectionView?.refreshControl?.tintColor = GUITheme.default.mintColor
        self.collectionView?.refreshControl?.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        self.collectionView?.alwaysBounceVertical = true
        self.collectionView?.backgroundColor = .groupTableViewBackground
    }
    
    
    //MARK: - Data management
    
    @objc private func reloadData() {
        fetchData(reload: true)
    }
    
    private func fetchData(reload: Bool) {
        if reload {
//            HUD.show(.progress, onView: self.view)
        }
        viewModel?.fetchData(reload: reload)
    }
}


//MARK: - UICollectionViewDataSource

extension HomeController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.itemsCount() ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ResourceCell.Constants.reuseIdentifier, for: indexPath) as! ResourceCell
        let cellViewModel = viewModel?.item(at: indexPath)
        cell.configure(with: cellViewModel)
        return cell
    }
    
}


//MARK: - UICollectionViewDelegate

extension HomeController {
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let viewModel = viewModel, indexPath.row == viewModel.itemsCount() - 1 {
            viewModel.fetchData(reload: false)
        }
    }

    override func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? ResourceCell {
            cell.didEndDisplaying()
        }
    }
}


//MARK: - UICollectionViewDelegateFlowLayout

extension HomeController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let columnsNumber = Constants.CollectionLayout.columnsNumber(traitCollection: self.traitCollection)

        var margin = collectionView.contentInset.left + collectionView.contentInset.right
        if #available(iOS 11.0, *) {
            margin += collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right
        } else {
            margin += collectionView.layoutMargins.left + collectionView.layoutMargins.right
        }
        margin += Constants.CollectionLayout.itemMargin(traitCollection: self.traitCollection) * (CGFloat(columnsNumber) - 1)

        var itemSize = collectionView.bounds.size
        itemSize.width = ((itemSize.width - margin) / CGFloat(columnsNumber)).rounded(.down)
        itemSize.height = itemSize.width

        return itemSize
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.CollectionLayout.itemMargin(traitCollection: self.traitCollection)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.CollectionLayout.itemMargin(traitCollection: self.traitCollection)
    }
}
