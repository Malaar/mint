//
//  PinboardHomable.swift
//  MintDemo
//
//  Created by Malaar on 6/6/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


typealias PinboardServiceCallback = ([Pinboard]?, Error?) -> Void


//MARK: - PinboardServiceError

enum PinboardServiceError: Error {
    case noData
    case parseObject
}


//MARK: - PinboardHomable protocol

protocol PinboardHomable {

    func homePinboards(offset: Int, limit: Int, callback: @escaping PinboardServiceCallback)
}
