//
//  PastebinService.swift
//  MintDemo
//
//  Created by Malaar on 6/6/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation
import Mint

class PinboardHomeService: PinboardHomable {
    
    private let baseURL: URL
    private let urlSession: URLSession
    private let mapper: PinboardMapper
    
    init(baseURL: URL, mapper: PinboardMapper) {
        self.baseURL = baseURL
        self.mapper = mapper
        urlSession = URLSession(configuration: URLSessionConfiguration.default)
    }
    
    func homePinboards(offset: Int, limit: Int, callback: @escaping PinboardServiceCallback) {
        let request = URLRequest(url: baseURL)
        let task = urlSession.dataTask(with: request) { [weak self] (data, response, error) in
            NetworkActivityIndicatorManager.instance.networkActivityIndicatorVisible(false)
            if error != nil {
                callback(nil, error)
                return
            }
            
            guard let data = data else {
                callback(nil, PinboardServiceError.noData)
                return
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
            if let itemsArray = json as? Array<Any> {
                var pinboards = [Pinboard]()
                
                // simulate pagin
                let from = min(offset*limit, itemsArray.count)
                let to = min(from + limit, itemsArray.count)
                let itemsArray = itemsArray[from..<to]
                
                for item in itemsArray {
                    do {
                        if let pinboard = try self?.mapper.pinboardFromObject(item) {
                            pinboards.append(pinboard)
                        }
                    } catch {
                        callback(nil, PinboardServiceError.parseObject)
                        return
                    }
                }
                callback(pinboards, error)
            }
        }
        task.resume()
        NetworkActivityIndicatorManager.instance.networkActivityIndicatorVisible(true)
    }
}
