//
//  MintTests.swift
//  MintTests
//
//  Created by Malaar on 6/5/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import XCTest
@testable import Mint

class InMemoryStorageTests: XCTestCase {
    
    var storage: InMemoryStorage!
    
    override func setUp() {
        super.setUp()
        storage = InMemoryStorage(configuration: InMemoryStorageConfiguration(maxCapacity: 10, maxMemoryUsage: 0))
    }
    
    override func tearDown() {
        storage = nil
        super.tearDown()
    }
    
    func testHasResource_False() {
        let hasResource = storage.hasResource(forKey: "someKey")
        XCTAssertFalse(hasResource, "Storage shouldn't have resource")
    }

    func testHasResource_True() {
        let identifier = "test", data = Data(count: 1024)
        let resource1 = CachableData(identifier: identifier, data: data)
        
        storage.setResourceSync(resource1, forKey: identifier)
        let hasResource = storage.hasResource(forKey: identifier)
        
        XCTAssertTrue(hasResource, "Storage should have resource")
    }
    
    func testSetResourceSync_Once() {
        let identifier = "test", data = Data(count: 1024)
        let resource1 = CachableData(identifier: identifier, data: data)

        storage.setResourceSync(resource1, forKey: identifier)
        let resource2 = storage.resourceSync(forKey: identifier)
        
        XCTAssertNotNil(resource2, "Added resource should exist")
    }
    
    func testSetResourceSync_SameIdentifier() {
        let identifier = "test"
        let resource1 = CachableData(identifier: identifier, data: Data(count: 1024))
        let resource2 = CachableData(identifier: identifier, data: Data(count: 2048))

        storage.setResourceSync(resource1, forKey: identifier)
        storage.setResourceSync(resource2, forKey: identifier)
        let currentCapacity = storage.currentCapacity
        
        XCTAssert(currentCapacity == 1, "Different resources with the same identifier should be added only once")
    }
    
    func testResourceSync_Nil() {
        let identifier = "UnexistResourceId"
        let resource = storage.resourceSync(forKey: identifier)
        XCTAssertNil(resource, "Resource should be nil")
    }

    func testResourceSync_Exist() {
        let identifier = "test", data = Data(count: 1024)
        let resource1 = CachableData(identifier: identifier, data: data)
        
        storage.setResourceSync(resource1, forKey: identifier)
        let resource2 = storage.resourceSync(forKey: identifier)

        XCTAssertNotNil(resource2, "Resource should exist")
    }
    
    func testCurrentMemoryUsage_Once() {
        let identifier = "id1"
        let dataSize = 1024
        let resource1 = CachableData(identifier: identifier, data: Data(count: dataSize))
        storage.setResourceSync(resource1, forKey: identifier)
        let memoryUsage = storage.currentMemoryUsage
        
        XCTAssert(memoryUsage == dataSize, "Memory usage should equal to: \(dataSize)")
    }
    
    func testCurrentMemoryUsage_Multiple() {
        let dataSize = 1024
        var totalSize = 0
        for i in 0 ..< 10 {
            let identifier = "id" + String(i)
            let resource = CachableData(identifier: identifier, data: Data(count: dataSize))
            storage.setResourceSync(resource, forKey: identifier)
            totalSize += dataSize
        }
        let memoryUsage = storage.currentMemoryUsage
        
        XCTAssert(memoryUsage == totalSize, "Memory usage should equal to: \(dataSize)")
    }
    
    func testMaxCapacity_Limitation() {
        for i in 0 ..< 2 * storage.configuration.maxCapacity {
            let identifier = "id" + String(i)
            let resource = CachableData(identifier: identifier, data: Data(count: 1024))
            storage.setResourceSync(resource, forKey: identifier)
        }
        
        let currentCapacity = storage.currentCapacity
        
        XCTAssert(currentCapacity == storage.configuration.maxCapacity, "Storage capacity should never be bigger than maxCapacity")
    }

    func testEvictItems() {
        var firstInsertedItemId = ""
        for i in 0 ..< storage.configuration.maxCapacity + 1 {
            let identifier = "id" + String(i)
            if i == 0 { firstInsertedItemId = identifier }
            let resource = CachableData(identifier: identifier, data: Data(count: 1024))
            storage.setResourceSync(resource, forKey: identifier)
        }
        
        let evictedResource = storage.resourceSync(forKey: firstInsertedItemId)

        XCTAssertNil(evictedResource, "First added resource should be evicted")
    }
}
