//
//  Mint.swift
//  Mint
//
//  Created by Malaar on 6/2/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation
import UIKit


//MARK: - ResourceCachable protocol

public protocol ResourceCachable {
    
    var identifier: String { get }
    var memorySize: Int { get }
    var data: Data? {get set}
}


//MARK: - ResourceImageCachable

public protocol ResourceImageCachable: ResourceCachable {
    
    var image: UIImage? { get }
}
