//
//  NetworkIndicatorManager.swift
//  Mint
//
//  Created by Malaar on 6/5/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


public class NetworkActivityIndicatorManager {

    public static let instance = NetworkActivityIndicatorManager()
    public var enable = true {
        didSet {
            if !enable {
                visibleCounter = 0
            }
        }
    }
    private var visibleCounter = 0
    
    public func networkActivityIndicatorVisible(_ visible: Bool) {
        DispatchQueue.main.async { [unowned self] in
            if !self.enable { return }
            
            if visible {
                self.visibleCounter += 1
            } else if self.visibleCounter > 0 {
                self.visibleCounter -= 1
            }
            UIApplication.shared.isNetworkActivityIndicatorVisible = self.visibleCounter > 0
        }
    }
    
    private init() { }
}
