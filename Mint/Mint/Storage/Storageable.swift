//
//  Storageable.swift
//  Mint
//
//  Created by Malaar on 6/4/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - Storageable protocol

public protocol Storageable {
    
    typealias CallbackEmpty = () -> Void
    typealias CallbackWithOptionalCachable = (ResourceCachable?) -> Void
    
    func hasResource(forKey identifier: String) -> Bool
    func resource(forKey identifier: String, callback: @escaping CallbackWithOptionalCachable )
    func resourceSync(forKey identifier: String) -> ResourceCachable?
    func setResource(_ resource: ResourceCachable, forKey identifier: String, callback: CallbackEmpty?)
    func setResourceSync(_ resource: ResourceCachable, forKey identifier: String)
    func removeAllResources()
}
