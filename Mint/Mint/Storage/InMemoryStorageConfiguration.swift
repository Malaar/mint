//
//  StorageConfiguration.swift
//  Mint
//
//  Created by Malaar on 6/6/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - InMemoryStorageConfiguration

public struct InMemoryStorageConfiguration {


    //MARK: - Defaults
    
    public struct Defaults {
        public static let maxCapacity = 100
        public static let maxMemoryUsage = 0
        static let queueName = "com.malaar.mint.storageQueue"
    }

    public let maxCapacity: Int
    public let maxMemoryUsage: Int
    
    public static var `default`: InMemoryStorageConfiguration = {
        return InMemoryStorageConfiguration(maxCapacity: Defaults.maxCapacity, maxMemoryUsage: Defaults.maxMemoryUsage)
    }()

    public init(maxCapacity: Int, maxMemoryUsage: Int) {
        self.maxCapacity = maxCapacity
        self.maxMemoryUsage = maxMemoryUsage
    }
}
