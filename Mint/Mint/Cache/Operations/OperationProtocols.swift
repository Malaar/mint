//
//  OperationProtocols.swift
//  Mint
//
//  Created by Malaar on 6/4/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - ResourceOperationable protocol

public protocol ResourceOperationable {
    
    var resource: ResourceCachable? { get set }
    var error: Error? { get set }
    
    func resourceFromDependentOperation() -> ResourceCachable?
    func errorFromDependentOperation() -> Error?
}

public extension ResourceOperationable where Self: Operation {
    
    func resourceFromDependentOperation() -> ResourceCachable? {
        guard let operation = dependencies.filter({$0 is ResourceOperationable}).first as? ResourceOperationable else { return nil }
        return operation.resource
    }
    
    func errorFromDependentOperation() -> Error? {
        guard let operation = dependencies.filter({$0 is ResourceOperationable}).first as? ResourceOperationable else { return nil }
        return operation.error
    }
}


//MARK: - SaveResourceOperationable protocol

public protocol SaveResourceOperationable: ResourceOperationable {
    
}


//MARK: - ProcessResourceOperationable protocol

public protocol ProcessResourceOperationable: ResourceOperationable {
    
    var resultResource: ResourceCachable? { get }
}


//MARK: - Operation types

public typealias ResourceOperationType = Operation & ResourceOperationable
public typealias SaveOperationType = Operation & SaveResourceOperationable
public typealias ProcessOperationType = Operation & ProcessResourceOperationable
