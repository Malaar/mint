//
//  ProcessResourceOperation.swift
//  Mint
//
//  Created by Malaar on 6/4/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - ProcessResourceOperation

open class ProcessResourceOperation: ResourceOperation, ProcessResourceOperationable {
        
    open var resultResource: ResourceCachable?
}
