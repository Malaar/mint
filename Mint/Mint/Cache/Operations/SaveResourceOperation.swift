//
//  SaveResourceOperation.swift
//  Mint
//
//  Created by Malaar on 6/4/18.
//  Copyright © 2018 Malaar. All rights reserved.
//

import Foundation


//MARK: - SaveResourceOperation

final public class SaveResourceOperation: ResourceOperation, SaveResourceOperationable {

    private let storage: Storageable

    public init(storage: Storageable) {
        self.storage = storage
        super.init()
    }
    
    public override func main() {
        if isCancelled { return }
        guard let resource = resource, resource.memorySize > 0 else { return }        
//        print("SAVING START")
        storage.setResourceSync(resource, forKey: resource.identifier)
//        print("SAVING COMPLETE")
    }
}
